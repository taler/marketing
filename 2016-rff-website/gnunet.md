GNUnet
======

GNUnet is a mesh routing layer for end-to-end encrypted networking and a
framework for distributed applications designed to replace the old insecure
Internet protocol stack.

In other words, GNUnet provides a strong foundation of free software for a
global, distributed network that provides security and privacy. Along with an
application for secure publication of files, it has grown to include all kinds
of basic applications for the foundation of a GNU internet.

GNUnet is an official GNU package. GNUnet can be downloaded from [GNU](ftp://ftp.gnu.org/gnu/gnunet/) and the
[GNU mirrors](ftp://ftpmirror.gnu.org/gnu/gnunet/).

Why GNUnet?
-----------

Security and privacy are virtually non-existant on today's Internet. Most
security protocols rely on some "trusted" third parties (such as certificate
authorities), which are frequently compromised. Traffic on the Internet is
routed and thus controlled by large providers, and easily censored (or blocked
entirely) at national borders. The originally decentralized web is increasingly
assimilated into services hosted at large advertising companies that mine it
for private information to be sold to advertisers. In all of these domains,
economics drives the creation of mega-corporations, which in turn has enabled
mass surveillance by authoritarian institutions.

GNUnet's goal is to offer a way out of hierarchical networking, transitioning
into a network of equals. As each participant contributes a small amount of
resources to the common, GNUnet does not require users to give up their privacy
in order to use the network. In particular, peer-to-peer networks do not need
advertising revenue or other profits to function. However, merely
decentralizing the Internet is not sufficient, we also need security and
privacy. In fact, for individuals, privacy tends to be more important among
peers than vis-a-vis a faceless mega-corporation. Thus, GNUnet's focus is on
providing security while remaining fully decentralized.

GNUnet is extensible and makes it easy to build new (peer-to-peer)
applications, or add alternative network transports to the base system. GNUnet
is not a classical overlay network, as GNUnet does not need TCP/IP to function:
while GNUnet peers can operate over classic Internet protocols like TCP or UDP,
they can also exchange information directly, for example over WLAN or
Bluetooth, without using IP at all.

GNUnet and Research
-------------------

GNUnet is not just an implementation effort, but also a research project.
Unlike many other research projects, our goal is to provide a working,
production-quality system (but we are not there yet). Providing security and
privacy in a fully decentralized setting is difficult, as many security
construction assume some kind of trusted third party. Furthermore, as GNUnet is
an open network, an adversary can actively participate. A strong adversary
might be able to run many peers, possibly even the majority of the peers. At
this point, even expensive security protocols that involve majorities fail. We
note that GNUnet does not have one "global" security model for all of its
applications; for that, our applications are too diverse in their designs and
requirements. However, we always build our components under the assumption that
there
are active, malicious participants in the network.

Many of the key technical contributions behind GNUnet are described in detail
in our research papers. The following sections will briefly sketch some of the
key ideas for some of GNUnet's flagship applications.


Anonymous File-sharing
----------------------

Key ideas behind GNUnet's anonymous file-sharing application include an
improved content encoding (ECRS, the encoding for censorship resistant sharing)
and a new protocol for anonymous routing (gap). In gap, anonymity is provided
by making messages originating from a peer indistinguishable from messages that
the peer is routing. All peers act as routers and use link-encrypted
connections with stable bandwidth utilization to communicate with each other.
Properties of the content encoding and the routing protocol allow GNUnet to
reward contributing peers with better service using an excess-based economic
model for resource allocation. As a result, peers in GNUnet monitor each others
behavior with respect to resource usage; peers that contribute to the network
are rewarded with better service.


The GNU Name System
-------------------

The GNU Name System (GNS) is a fully decentralized and censorship-resistant
public key infrastructure. Names in GNS are personal, as each user is in full
control of his ".gnu" zone. Users can delegate subdomains to the namespaces of
other users, and resolve each other's names using a privacy-preserving,
censorship-resistant secure network lookup mechanism. GNS is interoperable with
DNS, and can be used as an alternative to the X.509 PKI or the Web-of-Trust.


SecuShare
---------

Using GNS for identity management, we are currently starting to build the
foundation for fully decentralized social networking with SecuShare. Key design
goals include never storing (or transmitting) unencrypted data at third
parties, and the use of the PSYC protocol for semantic extensibility, that is,
to allow smooth migration of data to new revisions of the protocol. SecuShare
is still in its infancy, while we have worked out large parts of the design
much more code will need to be written before it can be used. Help is very
welcome. You can find additional details on the Secushare.org website.


Conclusion
----------

GNUnet continues to expand in scope and improve both in terms of technical
ideas and implementation, often thanks to discussions with developers from
related projects, such as Tor or I2P. We collaborate with these (and many
other) projects whenever it makes technical sense, as we share the same ideals
and goals: secure, private networking using free software to safeguard a free
and open society.

While we believe that GNUnet is (or at least will become) the best solution for
(anonymous) file-sharing, GNUnet is much more than this. Our developers have
the ambition to provide a good general infrastructure for developing a wide
range of new decentralized networking applications, possibly to the point of
replacing the Internet as it is known today with a GNU network that embodies
the ideals of the GNU project.

However, certain applications are not within the scope of the GNUnet project.
In particular, users that are looking for faster, non-anonymous file-sharing or
to anonymize their HTTP traffic should probably look elsewhere, as our goal is
not to duplicate existing applications.
