[GNU Taler](https://taler.net) is a new digital payment system whose
development is currently co-funded by the RFF.  GNU Taler aims to strike a
balance between radically decentralized technologies such as Bitcoin and
traditional payment methods while satisfying stricter ethical requirements such
as customer privacy, taxation of merchants and environmental consciousness
through efficiency.  GNU Taler also addresses micropayments, which are infeasible
with currently used payment systems due to high transaction costs.

Addressing the problem of micropayments is urgent. The overwhelming majority of
online journalists, bloggers and content creators currently depend on
advertisement revenue for their income.  The recent surge of ad-blocking
technology is threatening to destroy this primary source of income for many
independent online journalists and bloggers.  Furthermore the existing
advertisement industry is based on the Big Data business model, and users do
not only pay with their attention but also with private information about their
behavior. This threatens to move our society towards post-democracy.  Our goal
is to empower consumers and content creators by giving the choice to opt for
micropayments instead of advertisement.

Unlike many recent developments in the field of privacy-preserving online
payments, GNU Taler is not based on blockchain technology, but on Chaum-style
digital payments with additional constructions based on elliptic curve
cryptography.  Our work addresses practical problems that previous incarnations
of Chaum-style digital payments suffered from.  The system is entirely composed
of free software components, which facilitates adoption, standardization and
community involvement.

From the consumer's perspective, Taler's payment model comes closer to the
expectations one has when paying with cash than with credit cards.  Customers
do not need to authenticate themselves with personally identifying information
to the merchant or the payment processor.  Instead, individual payments are
authorized locally on the customer's computing device.  This rules out a number
of security issues associated with identity theft.  We expect that this will
also lower the barrier for online transactions due to the lower risk for the
customer.  With current payment solutions, the risk of identity theft
accumulates with every payment being made.  With our payment system, the only
risk involved with each individual payment is the amount being payed for that
single transaction.

In Taler, the paying customer is only required to disclose minimal private
information (as required by local law), while the merchant's transactions are
completely transparent to the state and thus taxable.  Taxable merely means
that the state can obtain the necessary information about the contract to levy
common forms of income, sales or value-added taxes, not that the system imposes
any particular tax code.  When customers pay, they use anonymized digital
payment tokens to sign a contract with the merchant. The digitally signed
contract is proposed by the merchant and is supposed to contain all the
information required for taxation -- which typically excludes the identity of
the customer.  The Taler payment system operator will wire funds to the
merchant's bank account after the merchant presented the tokens of value signed
by the customer.  The state can obtain the contract by following a chain of
cryptographic tokens, starting from a token in the wire transfer from the Taler
payment system operator to the merchant.  The payment system operator only
learns the total value of a contract, but no further details about the contract
or customer.

To pay with GNU Taler, customers need to install an electronic wallet on their
computing device.  Once such a wallet is present, the fact that the user does
not have to authenticate to pay fundamentally improves usability.  We already
see today that electronic wallets like GooglePay are being deployed to simplify
payments online. However, the dominant players mostly simplify credit card
transactions without actually improving privacy or security for citizens.  GNU
Taler is privacy-preserving free software and both technically and legally
designed to protect the interests of its users.

A demo of an online blog that uses GNU Taler is available at
<https://demo.taler.net/>.  Documentation for developers can be found at
<https://api.taler.net/>.

