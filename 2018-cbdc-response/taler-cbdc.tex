\documentclass{scrartcl}
\usepackage[a4paper,
top=2cm,
bottom=1cm,
includefoot,
left=4cm,
right=2cm,
footskip=1cm]{geometry}

\usepackage{lastpage} % enables \pageref{LastPage}

\usepackage{scrlayer-scrpage}
\cfoot*{\normalfont Page \pagemark{} of \normalfont \pageref{LastPage}}
\pagestyle{scrheadings}

% German indentation
\setlength{\parindent}{0pt}
\setlength{\parskip}{0.5ex plus 1pt minus 1pt}


\usepackage{graphicx}
%\usepackage{mathpazo}
\usepackage{newtxtext,newtxmath}
%\usepackage{mathptmx}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage[ngerman]{babel}
\usepackage{color}
\usepackage[hidelinks]{hyperref}

\begin{document}

\title{Taler as the Foundation \\ for a European Retail \\ Centrally Banked Digital Currency}
\author{Florian Dold}
\date{\today}
\maketitle%\vspace{-15ex}

This note elaborates on how the open source payment system GNU Taler fits into
the requirements of a Centrally Banked Digital Currencency (CBDC) intended for
use in the European retail market.  It was created as a response to an
unpublished draft note by an internal expert group of the European Central Bank's
InnovationLab.

\emph{Neither the original list of requirement nor this response reflects the
opinion of the ECB.  The ECB's official stance can be found in official
documents such as
\url{https://www.ecb.europa.eu/press/key/date/2017/html/sp170116.en.html}.}

\section*{Overview}
Taler Systems is developing GNU Taler, the software
infrastructure for an electronic payment system with focus on security,
efficiency and data minimization.  Cryptography is employed for security,
privacy by design and data minimization, but can at the same time guarantee that cash flows
to merchants/retailers are transparent for AML and other financial auditing
features.

The following components form the core of the system:
\begin{enumerate}
  \item An \emph{Electronic wallet} software stores cryptographic
    tokens of value (called digital coins), implemented via blind
    signatures.  Wallets are typically managed by the end user; a
    \emph{wallet provider} can manage storage of cryptographic
    material for the user, providing backup, synchronization and
    recovery.

  \item The \emph{Exchange} issues digital coins to wallets, after
    receiving money in a escrow account. The authorized electronic
    wallet is identified using an ephemeral \emph{reserve public key}
    encoded in the wire payment instructions.  As blind signatures are
    used, the exchange knows that it issued coins of a certain
    monetary value, but not to which wallet.  Digital coins are always
    denominated in a fiat currency (e.g.  Euro).

  \item The \emph{Merchant} proposes contracts to customers and
    receives payment in the form of contracts signed using digital
    coins. The merchant must then immediately clear these
    \emph{deposit permissions} with the exchange.  The exchange checks
    against double-spending, and if everything is in order provides
    the merchant with an instant \emph{deposit confirmation}.  After
    possibly aggregating many micro-transactions, the exchange sends
    money from the escrow account to the merchant's bank account.

  \item \emph{Auditors} are entities that certify which Exchanges can
    be trusted as legitimate.  Auditors must be configured in the
    electronic wallets and the merchants' infrastructure before these
    users accept digital coins of the respective exchanges.  Auditors
    include a software component used to conduct ongoing automated
    checks of the Exchanges' wire transaction history to detect if
    they deviate from their expected operation.  For this, auditors
    must be provided a replica of the exchange's database and read-only
    access to the escrow account.
\end{enumerate}

The implementation of all core components is licensed as free and open
source software (FOSS).

\section*{Addressing CBDC Requirements}

We now sketch how the Taler components map to a Centrally Banked Digital
Currency system run by the ECB or national central banks (NCBs), according to
the draft requirements.  Taler is a value-based payment system (as opposed to
an account-based system), and thus we will address the common requirements
C1-C8 and requirements V1-V4 specific to the value-based model.

\paragraph{C1. Tokenization:} \emph{Units of digital currency (CBDC units) are only created against money
blocked on a transit account, which will be held by ECB/NCBs}.

The ECB/NCBs would simultaneously take the role of the Taler Exchange
and Taler Auditor (or could outsource operations to qualified third parties).

\paragraph{C2. Issuance:} \emph{A central authority creates new CBDC units on
the reception of the transfer of an equivalent EUR amount from the
participating bank to the transit account. The same logic applies to the
destruction of existing CBDC units, where the central authority destroys CBDC
and releases EUR that were previously held by the ECB/NCBs in the transit
account.}

The ECB/NCBs create new CBDC units by issuing Taler digital coins,
and destroy CBDC units by accepting digital coin deposits from merchants, subsequently releasing
funds blocked in the escrow account and sending them to the merchant's bank account.

\paragraph{C4. 1-on-1 parity rule:} \emph{The parity rule applies when CBDC units are newly created or destroyed,
meaning that for each EUR blocked in (released from) the transit account there will be exactly
one CBDC created (destroyed). The parity rule also applies when CBDC are exchanged for
commercial bank deposits or physical cash, and vice versa.}

Digital coins in GNU Taler correspond 1-on-1 to a
value in a fiat currency such as the Euro.

\paragraph{C4. Two-tier structure:} \emph{The central authority issues CBDC only to entities entitled to deposit funds
in the transit account held at ECB/NCBs in exchange for newly issued CBDC units. Also, end-
users’ access to the CBDC payment system is intermediated via other entitled entities, acting as
gateways. All these entities, hereafter “tier-2 entities”, could be commercial banks or non-banks
(for example, payment service providers (PSPs), wallet providers etc.).}


With Taler, national banks could serve as
the primary Tier-2 entity, establish customer's identities (KYC) during bank
account setup, and facilitate the transfer from a customer's bank
account to the exchange's escrow account.  A secondary Tier-2 entity are the wallet providers.
Banks can serve as wallet providers, but other third party businesses could offer
a wallet backup/sync/restore services as well.  Customers are also given the option to be
responsible for the security of their wallet on their own, and manage private keys directly
and on their own device.


\paragraph{C5. Compliance with AML regulation:} \emph{Transactions with amounts above a certain threshold must be
disclosed to relevant parties as required by the AML regulation. In general, the system must be
designed in a way that discourages end-users from using it for anonymous large-value
transactions.}

Strict withdrawal limits can
be placed on customers' bank accounts.  Merchants can be required to collect
customer data for critical transactions.  Due to the technical measures
that provide transparency of cash flows to merchants, the compliance of
merchants is easy to verify.

\paragraph{C6. Fees:} \emph{The system should enable fee collection. The issuance of CBDC to banks and the
destruction of returned CBDC are free of charge for the entitled tier-2 entities (i.e. banks). Tier-2
entities can, however, charge fees to end-users for services they provide, such as their
involvement in the transfers of CBDC and/or the exchange of EUR into CBDC and vice versa.}

Taler has a flexible fee structure that is easily configured so that Tier-2 banks
can charge for CBDC creation and other activities.


\paragraph{C7. Availability:} \emph{Payments are processed 24 hours a day, 7 days a week, 365 days a year, without
operational downtimes.}

Taler requires no manual processing and can be made highly
available with standard software deployment and operations techniques.


\paragraph{C8. Throughput, transaction time and micropayments:} \emph{The
payment system must be able to handle a sufficiently large amount of
transactions. Each transaction must be processed real-time (to be compliant
with the SEPA Instant Credit Transfer (SCT Inst) scheme, the transaction time
would have to be maximum ten seconds). Furthermore, the payment system
should/could enable micropayments (low value, large volume, low cost, real time
transactions).}

Transactions
with Taler are processed in the order of milliseconds.  Unlike DLTs, Taler can
be easily scaled both horizontally (sharding, more processing nodes) and
vertically (faster machines).  Since multiple payments to a merchant can be aggregated into
one bank transfer, even micropayments with fractions of a cent are possible.  All coins
are issued with expiration dates, ensuring that the exchange may eventually delete ancient
transactions.

\paragraph{V1. Non-interest-bearing:} \emph{In the value-based model, holdings of CBDC do not bear interest - neither
positive nor negative.}

In Taler, digital coins do not bear interest; however,
when coins expire it is possible to charge fees when the electronic wallets trade
expiring coins for fresh coins. This feature may be used to
provide a mechanism for negative interest rates (for non-circulating coins).


\paragraph{V2. Limitation of bank runs:} \emph{In the value-based model, to avoid a situation, in which end-users
(suddenly) shift large amounts of their commercial bank deposits to CBDC, daily (potentially also
weekly or monthly) limits should be imposed on the amount that can be converted from
commercial bank deposits into CBDC.}

Bank runs are discouraged and limited with Taler:  (1) Withdrawal
limits can be imposed by the Tier-2 banks on the withdrawal of CBDC units; (2) wallet providers may place limits
on how much money can be stored in online wallets; (3) customers that mange their own wallet are discouraged from
storing large amounts of CBDC units in their wallets, as they must ensure its safety similar to a physical wallet;
(4) modest expiration times with modest refresh fees make hoarding coins unattractive.


\paragraph{V3. Anonymity and AML:} \emph{The system should allow anonymous low-value transactions (below a
certain amount used as threshold). Moreover, it should be possible to trace large-value
transactions and link them to the identities of the participants (through KYC). Furthermore, as
countermeasure against splitting large-value transactions into multiple low-value anonymous
transactions, it should be possible to identify multiple low-value transactions which are
processed within a certain period of time and which sum up to an amount greater than the
chosen threshold.}

The exchange does not know which customer owns which coin
due to the use of blind signatures during the withdrawal process.
AML measures are based on the \emph{income transparency} feature,
where cash flows to merchants are visible to the exchanges (and
thus ECB/NCBs).  As the merchant redeems CBDC units with a transaction to their bank account, the KYC process
already happened when the merchant opened their SEPA bank account.  Furthermore, the
deposit permissions are linked to the contract with the customer, allowing authorities
to validate the plausiblity of the transaction during tax audits.
With Taler, ownership of digital coins between mutually distrusting parties can only be securely transferred with a digital coin deposit via the exchange.
This discourages ``invisible'' payments by sharing digital coins between wallets
without involving the exchange.

\paragraph{V4. Ownership and spending rights of CBDC:} \emph{In the value-based model, units of CBDC are held by
end-users themselves. Each end-user has cryptographic information (e.g. private keys, other
secrets) without which CBDC units associated with that particular cryptographic information
material cannot be spent. Spending rights are defined by technology (e.g. if you have private
keys you can spend).}

Technically literate
users have the option to manage their own wallets and private keys, whereas
other users can use wallet backup/sync/restore providers.

\section*{Contrast and Relationship to DLT-based Systems}

The Taler payment system is independent from Distributed Leder
Technology (DLT) systems.  In particular, Taler payments are not
necessarily backed by any blockchain or cryptocurrency.  Even though
Taler uses cryptographically secured payment tokens, it is distinct
from ``cryptocurrencies'': Taler is a very efficient electronic
payment system with certain characteristics like cash, but it is not a
currency.  Taler is designed to serve as a payment instrument for
retail commerce, in contrast to DLTs which are generally used more as
a long-term stores-of-value or as speculative assets.

Some technological advancements made by DLTs could potentially benefit Taler.
For example, public cryptographic key material and data relevant for auditing
could be further secured by a distributed ledger.  Yet a distributed ledger is
not mandatory to operate Taler, as payments are facilitated by a federation of
trusted entities, with oversight from each other and/or a central institution,
not too dissimilar from how traditional banking systems work.

\end{document}
