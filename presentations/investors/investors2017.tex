\documentclass[c]{beamer}

%\usepackage{helvet}
\usepackage{calc}
\usepackage[utf8]{inputenc} % set your input encoding differently, if you want
\usepackage[english]{babel}

\usepackage{enumitem}
\usepackage{eurosym}
\usepackage{tikz}
\usepackage{amsmath,amssymb}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usepackage[absolute,overlay]{textpos}

%\setbeameroption{show notes}

\setlist[itemize]{label=$\bullet$}


\def\checkmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}

\setbeamercovered{transparent=10}
 \setbeamertemplate{navigation symbols}{Investor presentation --- Taler Systems S.A. --- \today --- Page \insertpagenumber}
\setbeamertemplate{section in toc}[sections numbered]

% Adapt title information
% =======================
\title{Taler Systems S.A. \\ {\small {\bf T}axable {\bf A}nonymous {\bf L}ibre {\bf E}lectronic {\bf R}eserves}}
\institute{Instant Independent One-Click Payments}
\author{Christian Grothoff \& Leon Schumacher}
\date{\today}

% Some common packages
% ====================
\usepackage{units}
\usepackage{amsbsy}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphics}
\usepackage{epsf}
\usepackage{epsfig}
\usepackage{fixmath}
\usepackage{wrapfig}


 \usetikzlibrary{snakes}


\begin{document}

\begin{frame}
  \titlepage

{\tiny
``I think one of the big things that we need to do, is we need to get a way from true-name payments on the Internet. The credit card payment system is one of the worst things that happened for the user, in terms of being able to divorce their access from their identity.'' \\ \hfill --Edward Snowden, IETF 93 (2015)}
\end{frame}

\begin{frame}{Agenda}
  \tableofcontents
\end{frame}

\section{What is Taler?}

\begin{frame}{1. The Problem}

  \begin{textblock*}{3cm}(0.5cm,1cm) % {block width} (coords)
    \begin{center}
      \includegraphics[width=\textwidth]{illustrations/cc3ds.pdf}

      3D Secure\note<1>{Using 3D secure (verified by visa) is a nightmare for customers.
                     The process is complicated, takes a long time to complete and
                     can refuse valid payment requests with obscure errors.

                     As a result, cryptographers write short papers with titles like:
                     ``Verified by Visa and MasterCard SecureCode:
                     Or, How Not to Design Authentication''.

                     Todays online credit card payments will be replaced, but with what?}
    \end{center}
\end{textblock*}
\begin{textblock*}{3cm}(5cm,4cm) % {block width} (coords)
      \includegraphics[width=2.5cm]{illustrations/bank.pdf}
\end{textblock*}
\begin{textblock*}{4cm}(9.5cm,3.2cm) % {block width} (coords)
 \only<3,4,5>{\includegraphics[width=\textwidth]{competitor-logos/amazon.png}}
\end{textblock*}
\begin{textblock*}{2cm}(7cm,1cm) % {block width} (coords)
 \only<3,4,5>{\includegraphics[width=\textwidth]{competitor-logos/alipay.jpeg}}
\end{textblock*}
\begin{textblock*}{2cm}(9cm,0.5cm) % {block width} (coords)
 \only<3,4,5>{\includegraphics[width=\textwidth]{competitor-logos/paypal.jpeg}}
\end{textblock*}
\begin{textblock*}{2cm}(9cm,2cm) % {block width} (coords)
 \only<3,4,5>{\includegraphics[width=\textwidth]{competitor-logos/applepay.jpeg}}
\end{textblock*}
\begin{textblock*}{2cm}(7.5cm,2.9cm) % {block width} (coords)
 \only<3,4,5>{\includegraphics[width=\textwidth]{competitor-logos/samsungpay.jpeg}}
\end{textblock*}
\begin{textblock*}{1cm}(9.5cm,3.3cm) % {block width} (coords)
 \only<3,4,5>{\includegraphics[width=\textwidth]{competitor-logos/android_pay.png}}
\end{textblock*}
\begin{textblock*}{4cm}(4.5cm,7cm) % {block width} (coords)
  \begin{center}
\only<2,3,4,5>{\includegraphics[width=2cm]{illustrations/ec.png}

  AML, KYC}\only<5>{, {\bf GDPR}
}
  \end{center}
\end{textblock*}
\begin{textblock*}{3cm}(9cm,6cm) % {block width} (coords)
 \only<4,5>{\includegraphics[width=2cm]{competitor-logos/laterpay.jpeg}}
\end{textblock*}
\begin{textblock*}{3cm}(9cm,7cm) % {block width} (coords)
 \only<4,5>{\includegraphics[width=2cm]{competitor-logos/sofortueberweisung.jpeg}}
\end{textblock*}
\begin{textblock*}{3cm}(9cm,8cm) % {block width} (coords)
 \only<4,5>{\includegraphics[width=2cm]{competitor-logos/paydirect.jpeg}}
\end{textblock*}
\begin{textblock*}{3cm}(8.5cm,6cm) % {block width} (coords)
 \only<5>{\includegraphics[width=3cm]{illustrations/ban.pdf}}
\end{textblock*}
 \note<2>{Any technology deployed by European banks also needs to
   be compliant with applicable regulation, such as anti-money laundering
   and know-your-customer rules.}
\note<3>{Foreign technological providers will use their established
  brands and platforms to both ignore regulations (see ``safe harbor'')
  and to enthrall customers with payment solutions that exploit the
  ``trusted'' platform.  Amazon can offer one-click payments today,
  but smaller business cannot.

  Eventually, these providers will obtain banking licenses.

  The day Google attaches a bank account to every Gmail account
  offering one-click payments via all Android devices and free
  personal banking will be the beginning of the
  end of European banking (and economic/political independence).}
\note<4>{European competitors fail to be global, and are technologically
  and economically disadvantaged compared to the global brands.  They
  also are decades behind the technical state of the art. Cryptographers
  (or CCC hackers) will write more papers.

  Some banks will continue to follow the blockchain meme, ignoring
  performance, scalability, compliance or usability concerns.

  (Blockchains are P2P protocols implementing Byzantine consensus
   using computationally expensive puzzles for leader election.
   Our research team has worked on secure P2P protocols for over
   15 years and published papers on Byzantine consensus in 2016.
   While we are thus subject-experts, Taler does aggregation at the payment provider,
   thus rarely needs multi-party consensus and simply relies on
   traditional wire transfers (including possibly Bitcoin) to
   settle in aggregate.)}
\note<5>{Once the EU's general data protection regulation mandating
  privacy-by-design and data minimization is enforce, all of these
  providers will be out of compliance, as none implement strict data
  minimization or privacy-by-default.

  Non-EU providers will continue their quest for dominance and
  operate extra-legally, extra-teritorially if necessary, ready
  to take over the market once regulation allows it.}
\end{frame}


\begin{frame}{2. What is Taler?}
  \vfill
  \begin{center}
Taler is an instant electronic payment system.
  \end{center}
  \begin{itemize}
  \item Pay in existing currencies (i.e. EUR, USD)
  \item Uses electronic coins stored in wallets on customer's device
  \end{itemize}
  \vfill
\end{frame}


\begin{frame}
\frametitle{2. What is Taler?}
\begin{center}
  \includegraphics[width=0.9\textwidth]{illustrations/taler-arch-full.pdf}

  $\Rightarrow$ Convenient, taxable, privacy-enhancing, \& resource friendly!
\end{center}
\note{Taler is a modern payment system based on cryptography, providing
  an open standard with a free software reference implementation.
  Taler avoids authentication during payments, enabling convenient and fast
  one-click payments in compliance with applicable regulation (AML, KYC and GDPR).

  We have a public demonstrator running at \url{https://demo.taler.net/}.}
\end{frame}


\begin{frame}{2. Advantages of Taler}
    \begin{itemize}
        \item{All operations provide cryptographically secured, mathematical proofs for courts \& auditors}
        \item{Customer can remain anonymous
          \begin{itemize}
          \item retain civil liberties in increasingly cash-less world
          \item eliminates costly customer authentication
          \item no credit card number theft possible
          \item merchants do not need to operate expensive certified equipment \& processes (PCI DSS, etc.)
          \item Taler can give change and refunds, even to anonymous customers
          \end{itemize}}
        \item{Merchants are identifiable in each payment they receive
          \begin{itemize}
          \item bad for illegal business
          \item no tax evasion
        \end{itemize}}
    \end{itemize}
\end{frame}


\begin{frame}{2. Advantages of Taler}
    \begin{itemize}
        \item{Payments in existing currencies, does not introduce any new currency
          \begin{itemize}
          \item  financial stability, no risks from currency fluctuation
          \item  payment system, not speculative investment
        \end{itemize}}
        \item{Scalable, fast protocol implementation
          \begin{itemize}
          \item  low transaction costs (in terms of computation at high volume)
          \end{itemize}}
        \item{Open standard protocol without patents with free reference implementations
          \begin{itemize}
          \item  low barrier to entry for new merchants
          \item  governments may adopt as part of digital sovereignty agenda
          \end{itemize}}
    \end{itemize}
\end{frame}



\begin{frame}{3. Operating Model}
  \begin{center}
\includegraphics[width=\textwidth]{illustrations/operating-model.pdf}
  \end{center}
  \note{Operator charges transaction fees.
    Taler Systems develops core technology and provides
    exclusive support to payment service provider.

    In return, payment service provider contributes
    financially to Taler Systems S.A.}
\end{frame}


\section{The Market}
% FIXME: update, include Visa/Master/PayPal, AliPay in particular!
% FIXME: render more nicely?
\begin{frame}{4. The Market}
 \begin{textblock*}{0.6\textwidth}(1cm,5cm) % {block width} (coords)
 \includegraphics[width=\textwidth]{payment_solutions.png}
\end{textblock*}
\begin{textblock*}{0.6\textwidth}(5.5cm,1cm) % {block width} (coords)
 \includegraphics[width=\textwidth]{payment_volume.png}
\end{textblock*}
\note{The market is big.}
\end{frame}


\begin{frame}{5. Competitor comparison}
  \begin{center} \small
    \begin{tabular}{l||c|c|c|c|c}
                & Cash & Bitcoin & Zerocoin & Creditcard & GNU Taler \\ \hline \hline
   Online      &$-$$-$$-$  &   ++    &    ++    &     +      &   +++  \\ \hline
    Offline     & +++  &   $-$$-$    &    $-$$-$    &     +      &   $-$$-$  \\ \hline
    Trans. cost & +    & $-$$-$$-$   & $-$$-$$-$  &     $-$      &   ++  \\ \hline
    Speed       & +    & $-$$-$$-$   & $-$$-$$-$  &     o      &   ++  \\ \hline
    Taxation    & $-$    &   $-$$-$    &  $-$$-$$-$   &    +++     &  +++  \\ \hline
    Payer-anon  &  ++  &   o     &    ++    &  $-$$-$$-$   &  +++  \\ \hline
    Payee-anon  & ++   &   o     &    ++    &  $-$$-$$-$    &  $-$$-$$-$ \\ \hline
    Security    &  $-$   &   o     &    o     &    $-$$-$      &  ++   \\ \hline
    Conversion  & +++  &  $-$$-$$-$   & $-$$-$$-$ &    +++     &  +++  \\ \hline
    Libre       &  $-$   &  +++    &    +++   & $-$ $-$ $-$      &  +++  \\
  \end{tabular}
  \end{center}
  \note{Not having payee-anonymity is a good thing as otherwise money laundering
    becomes a real problem.}
\end{frame}


\begin{frame}{6. Payment solutions - Pricing}
  % FIXME: Konkrete Zahlen fuer Beispieltransaktionen
  \begin{center}
  \begin{tabular}{l||c}
    {\bf Provider}  & {\bf Pricing} \\ \hline \hline
    Alipay          & 2,0\% - 3,0\% \\ \hline
    Allied Wallet   & 1,95\% + \$ 0,20 \\ \hline
    Amazon Payments & 2,9\% + \$ 0,30 \\ \hline
    Avangate        & 4,9\% + \$ 2,50 \\ \hline
    Billpro         & 2,1\% + 3,5\% fee \\ \hline
    BitGold Inc.    & 1\% fee on every purchase \\ \hline
    Bitpay (Bitcoin)&  0\% \\ \hline
    Checkout.com    & 2,95\% - 3,95\% + \pounds 0,15 \\ \hline
    Coinify (Bitcoin) & 0\% \\ \hline
    eComCharge      & 3,5\% + \EUR{0,35} \\ \hline
    GoCardless      & 1\% up to a maximum of \pounds 2 \\ \hline
    Western Union   & Variable --- From 5\% up
  \end{tabular}
  \end{center}
  \note{Transaction cost in Taler is $\approx$ \EUR{0.0001} (plus salaries and profits).}
\end{frame}


\section{About Us}

\begin{frame}
  \frametitle{7. Why now and why us?}
  \begin{block}{Why now?}
    {%\tiny
  \begin{itemize}
    \item{Chaum's original patents\footnote{\tiny USPTO 5878140, 5781631, 5712913} from 1996-1999 have expired}
    \item{Increased awareness of issue of privacy in payment systems {\tiny
    \begin{itemize}
    \item Contemporary payment systems fail on privacy
    \item Cash is disappearing
    \item Alternatives urgently needed
    \end{itemize}}}
    \item{Cryptocurrencies threaten control over money supply and tax base of governments}
  \end{itemize}
  }
  \end{block}
  \begin{block}{Why us?}
    {%\tiny
    \begin{itemize}
    \item{solved (technical) problem of unlinkability}
    \item{designed a modern, open standards based version}
    \item{technical expertise to really build it: {\tiny
      \begin{itemize}
      \item{15 years of research in network security and privacy}
      \item{Founder of GNUnet and related projects}
    \end{itemize}}}
    \item{good contacts: free software movement, press, academics}
    \end{itemize}
    }
\end{block}
\end{frame}


\begin{frame}
  \frametitle{8. Team \hfill \& \hfill Advisory Board}
  \begin{minipage}{5cm}
    \setlist[description]{leftmargin=1cm,labelindent=0cm}
    \begin{description}
 \item[Leon Schumacher]\ \\ CEO, co-founder
 \item[Dr. Christian Grothoff]\ \\ CTO, co-founder
 \item[Dr. Jeff Burdges]\ \\ PostDoc
 \item[Dr. Christina Onete]\ \\ PostDoc
 \item[Florian Dold]\ \\ PhD student
    \end{description}

\end{minipage}
  \begin{minipage}{5cm}
{\small
\begin{description}
 \item[Prof. Mikhail Atallah] \ \\
   Cryptographer, co-founder
   Arxan Technologies Inc.
 \item[Prof. Roberto Di Cosmo] \ \\
   Director IRILL
 \item[Greg Framke] \ \\
   CIO Manulife, \\
   former COO Etrade
 \item[Dr. Richard Stallman]\ \\
   Founder of the \\ \mbox{Free Software movement}
 \item[Chris Pagett] \ \\
   former Group Head Security/ \ \\
   Fraud/Geo Risk HSBC
 \item[Ante Gulam] \ \\
   Global Head of Information Security \\
   MetaPack Group
\end{description}
}
\end{minipage}
\vfill
\includegraphics[height=0.1\textwidth]{team-images/leon-schumacher.jpg}    \hfill
\includegraphics[height=0.1\textwidth]{team-images/christian-grothoff.jpg}\hfill
\includegraphics[height=0.1\textwidth]{team-images/jeff-burdges.jpg}\hfill
\includegraphics[height=0.1\textwidth]{team-images/cristina-onete.jpg}\hfill
\includegraphics[height=0.1\textwidth]{board-images/mja.jpg} \hfill
\includegraphics[height=0.1\textwidth]{board-images/roberto-di-cosmo.jpg}    \hfill
\includegraphics[height=0.1\textwidth]{board-images/greg-framke.jpg}    \hfill
\includegraphics[height=0.1\textwidth]{board-images/richard-stallman.jpg}\hfill
\includegraphics[height=0.1\textwidth]{board-images/ante-gulam.jpg}
\note{Advisory board still under construction.}
\end{frame}


\section{The Plan}

\begin{frame}{9. Status quo and plan}
  \begin{tikzpicture}[snake=zigzag, line before snake = 5mm, line after snake = 5mm]
    % draw horizontal line
    \draw (0,0) -- (6,0);
    \draw[snake] (6,0) -- (8,0);

    % draw vertical lines
    \foreach \x in {0,1,2,3,4,5,6,8}
      \draw (\x cm,3pt) -- (\x cm,-3pt);

    % draw nodes
    \draw (0,0) node[below=3pt] {today} node[above=3pt, rotate=45, anchor=south west] {Public release and demo};
    \draw (1,0) node[below=3pt] { 6 m} node[above=3pt, rotate=45, anchor=south west] {Bank and investor signed};
    \draw (2,0) node[below=3pt] {12 m} node[above=3pt, rotate=45, anchor=south west] {First bank operational};
    \draw (3,0) node[below=3pt] {18 m} node[above=3pt, rotate=45, anchor=south west] {Major publisher operational};
    \draw (4,0) node[below=3pt] {24 m} node[above=3pt, rotate=45, anchor=south west] {Mobile app launched};
    \draw (5,0) node[below=3pt] {30 m} node[above=3pt, rotate=45, anchor=south west] {Financial break even (EU)};
    \draw (6,0) node[below=3pt] {36 m} node[above=3pt, rotate=45, anchor=south west] {Expansion to US market};
    \draw (8,0) node[below=3pt] {60 m} node[above=3pt, rotate=45, anchor=south west] {Global operation};
  \end{tikzpicture}
  \noindent
  \begin{itemize}
    \item[500 k]{Bank operational (integration, documentation, auditing)}
    \item[500 k]{Publisher operational (training, integration, marketing)}
    \item[500 k]{Mobile app for major platforms (incl. NFC protocol)}
    \item[500 k]{Operational overheads until break even}
  \end{itemize}
  \noindent
      {\small
        \begin{tabular}{l||r|r|r|r|r||r}
                              & Year 1 & Year 2 & Year 3 & Year 4 & Year 5 & Invest \\ \hline
          EBIT                &  -208  & -2.666 & -2.902 &  1.689 & 9.542  & 5 M    \\% \hline
%          EBIT                &  -104  &   -290 &   -77  &    72  &  394   & 500 k  \\
%          EBIT (optimistic)   &  -208  & -2.666 & -2.902 &  1.689 & 9.542  & 5 M    \\ \hline
%          EBIT (conservative) &  -104  &   -290 &   -77  &    72  &  394   & 500 k  \\
        \end{tabular}
        }
\end{frame}


\section{Supplemental Materials}

\begin{frame}{10. Contact details}
  \begin{center}
    \begin{tabular}{p{5cm}p{5cm}}
      {\bf CEO}               & {\bf CTO}  \\ & \\
  Leon Schumacher             & Dr. Christian Grothoff \\
  {\tt ceo@taler.net}         & {\tt cto@taler.net} \\
  +41-79-865-9365             & +33-2-99-84-71-45   \\
%  \url{http://taler.net/ceo/} & \url{http://taler.net/cto/} \\
    \end{tabular}

    \vspace{2cm}
    \url{https://www.taler.net/}
  \end{center}
\end{frame}


\begin{frame}
  \begin{center}
    \includegraphics[width=0.66\textwidth]{taler-logo-2017.pdf}
  \end{center}
\end{frame}

%\end{document}

\begin{frame}{Partners}
  \begin{minipage}{5cm}
    {\bf Research and development:}
\begin{center}
      \includegraphics[height=1cm]{partner-logos/inria.png}
      \includegraphics[height=1cm]{partner-logos/tum.png}

      \includegraphics[height=1cm]{partner-logos/gnu.jpeg}
      \includegraphics[height=1cm]{partner-logos/gnunet.jpeg}
\end{center}

\vspace{0.1cm}

    {\bf Business development:}

\begin{center}
    \includegraphics[height=1cm]{partner-logos/ashoka.png}
    \includegraphics[height=1cm]{partner-logos/hl.png}
\end{center}

      \vspace{0.1cm}

   {\bf Strategic partners:}

 \begin{center}
    \includegraphics[height=0.8cm]{partner-logos/rff.png}
    \includegraphics[height=1cm]{partner-logos/pep.pdf}
    \includegraphics[height=1cm]{partner-logos/tor.jpeg}
    \includegraphics[height=1cm]{partner-logos/tcij.png}
\end{center}
 \end{minipage}
  \hfill
  \begin{minipage}{5cm}
%  {\bf Banks (in discussion):}

 %     \includegraphics[height=1cm]{partner-logos/hsbc.jpeg}
%      \includegraphics[height=1cm]{partner-logos/gls-bank.png}
%  \vfill
%    \vspace{1cm}

  {\bf Merchants:}

  \includegraphics[height=0.8cm]{partner-logos/dk.png}

  \includegraphics[height=1cm]{partner-logos/spiegel.jpeg}
  \includegraphics[height=1cm]{partner-logos/heise.png}
    \vfill
  \end{minipage}
\end{frame}


\begin{frame}{Recent Press}
  {\tiny
  \setlist[description]{leftmargin=1cm,labelindent=0.5cm}
  \begin{description}
  \item[09-2015]\ \\
    \begin{itemize}
  \item \url{https://bitcoinmagazine.com/21901/bitcoin-governments-without-privacy-taxes/}
  \item \url{http://www.heise.de/tp/artikel/46/46089/1.html}
    \end{itemize}
  \item[06-2016]\ \\
    \begin{itemize}
   \item \url{http://www.theregister.co.uk/2016/06/06/gnu_cryptocurrency_aims_at_the_mainstream_economy_not_the_black_market/}
   \item \url{http://www.golem.de/news/halbanonymes-bezahlsystem-gnu-taler-soll-kryptowaehrungen-gerechter-machen-1606-121323.html}
   \item \url{http://www.heise.de/newsticker/meldung/GNU-Taler-Open-Source-Protokoll-fuer-Zahlungen-in-Version-0-0-0-erschienen-3228525.html}
    \end{itemize}
  \item[08-2016]\ \\
    \begin{itemize}
    \item \url{https://www.theguardian.com/technology/2016/sep/01/online-publishers-readers-ad-block-surveillance-donate-anonymously}
    \end{itemize}
   \item[02-2017]\ \\
    \begin{itemize}
    \item \url{http://hackerpublicradio.org/eps.php?id=2222}
    \end{itemize}
  \end{description}
  }
  \begin{center}
    Complete list at \url{https://taler.net/press}
  \end{center}
\end{frame}


\begin{frame}{Business risks and measures}
  \begin{center} \tiny
  \begin{tabular}{p{3cm}|p{3cm}|p{3cm}}
    {\bf Risk}                & {\bf Impact}                   & {\bf Countermeasure} \\ \hline \hline
    Usability too low         & few users, insufficient income & usability testing \\ \hline
    Exchange data loss        & financial damage               & backups \\ \hline
    Exchange compromise       & financial damage               & limit loss by key rotation \\ \hline
    Exchange offline          & reputation loss                & redundant \mbox{operation} \\ \hline
    Compliance issues         & illegal to operate             & work with \mbox{regulators} \\ \hline
    No bank license           & illegal to operate             & work with banks  \\
  \end{tabular}
  \end{center}
\end{frame}


\begin{frame}{License model}
  \begin{center}
  \begin{tabular}{p{2cm}|c|p{5.5cm}}
    {\bf Component}    & {\bf License}    & {\bf Why?} \\ \hline \hline
    Exchange           & Affero GPL       & competitors must share \mbox{extensions} \\ \hline
    Wallets            & GPLv3+           & enable community development \\ \hline
    Merchant           & Lesser GPL       & maximize market adoption   \\
  \end{tabular}
  \end{center}
\end{frame}


\section{Financials}

\begin{frame}{Balance Sheet: Year 1, 2, 3 (kEUR)}
\scalebox{0.8}{
  \tiny
  \begin{tabular}{l|r|r|r|c|l|r|r|r}
                                     & Year 1 & Year 2 & Year 3 & &                                   & Year 1 & Year 2 & Year 3 \\ \hline
    {\bf Fixed assets}               &        &        &        & & {\bf Equity}                      &        &        &        \\ \hline
    Incorporation costs              &      5 &      4 &      3 & & Share capital                     &     45 &     45 &     45 \\ \hline
    Intangible assets                &     16 &     63 &     40 & & Share premium                     &  5'350 & 10'350 & 10'350 \\ \hline
    Tangible assets                  &     20 &     44 &     68 & & Reserves                          &        &        &        \\ \hline
    Financial assets                 &      0 &      0 &      0 & & Retained earnings                 &      0 & -1'477 & -4'987 \\ \hline
    {\bf Current assets}             &        &        &        & & Profit/loss for the year          & -1'477 & -3'510 & -1'500 \\ \hline
    Inventory                        &        &        &        & & Provisions                        &      0 &     23 &    168 \\ \hline
    Accounts receivable (clients)    &        &     30 &    200 & & {\bf Long-term debts $>$ 1 year}  &        &        &        \\ \hline
    Accounts receivable (affiliated) &        &        &        & & Bank loans                        &      0 &      0 &      0 \\ \hline
    Other current assets             &        &        &        & & Accounts payable (affiliated)     &      0 &      0 &      0 \\ \hline
    Cash in bank                     &  3'898 &  5'364 &  3'840 & & {\bf Short-term debts $<$ 1 year} &        &        &        \\ \hline
                                     &        &        &        & & Bank loans                        &      0 &      0 &      0 \\ \hline
                                     &        &        &        & & Accounts payable (suppliers)      &      0 &      0 &      0 \\ \hline
                                     &        &        &        & & Fiscal and social debt            &     21 &     74 &     75 \\ \hline
                                     &        &        &        & & Accounts payable (affiliated)     &      0 &      0 &      0 \\ \hline
                                     &        &        &        & & Other liabilities                 &      0 &      0 &      0 \\ \hline
    {\bf TOTAL ASSETS}               &  3'939 &  5'505 &  4'151 & & {\bf TOTAL EQUITY + LIABILITIES}  &  3'939 &  5'505 &  4'151 \\
\end{tabular}}
\vfill
\begin{center}
  Projections for years 4--6 available upon request.
\end{center}
\end{frame}


\begin{frame}{Income Statement: Year 1, 2, 3 (kEUR)}
  \small
  \begin{tabular}{l|r|r|r}
    {\bf Profit or loss}                             & Year 1 & Year 2 & Year 3 \\ \hline
                                                     &        &        &        \\ \hline
    Turnover                                         &      0 &    129 &  2'520 \\ \hline
    Other operating income                           &      0 &      0 &      0 \\ \hline
    Cost of goods sold                               &      0 &      0 &      0 \\ \hline
    Personnel costs                                  & -1'066 & -2'804 & -3'085 \\ \hline
    Value adjustments                                &      0 &      0 &      0 \\ \hline
    Other operating charges                          &   -412 &   -835 &   -935 \\ \hline
    {\bf Operating result}                           & -1'477 & -3'510 & -1'500 \\ \hline
    Interest \& similar income                       &      0 &      0 &      0 \\ \hline
    Interest \& similar charges                      &      0 &      0 &      0 \\ \hline
    {\bf Financial result}                           &      0 &      0 &      0 \\ \hline
    {\bf Result from ord. act. before taxation}      & -1'477 & -3'510 & -1'500 \\ \hline
    Extraordinary income                             &      0 &      0 &      0 \\ \hline
    Extraordinary expenses                           &      0 &      0 &      0 \\ \hline
    {\bf Extraordinary result}                       &      0 &      0 &      0 \\ \hline
    Taxes                                            &      0 &      0 &      0 \\ \hline
    {\bf Profit or loss fiscal year}                 & -1'477 & -3'510 & -1'500 \\ \hline
  \end{tabular}
\end{frame}


\begin{frame}{Cashflow Statement: Quarter 1 to 12 (kEUR)}
\scalebox{0.7}{
  \tiny
  \begin{tabular}{l|c|r|r|r|r|r|r|r|r|r|r|r|r}
    {\bf Cash flow (Q1-Q12)}           & &  Q1    &   Q2    &   Q3    &   Q4    &   Q5    &   Q6    &   Q7    &   Q8    &   Q9    &  Q10    &  Q11    &   Q12   \\ \hline
                                       & &        &         &         &         &         &         &         &         &         &         &         &         \\ \hline
    {\bf Beginning Cash Balance ($A$)} & &      0 &     298 &   5'065 &   4'561 &   3'898 &   3'084 &   2'214 &   1'263 &   5'364 &   4'633 &   4'107 &   3'794 \\ \hline
    {\bf Cash Inflows}                 & &        &         &         &         &         &         &         &         &         &         &         &         \\ \hline
    Turnover (inc VAT)                 & &      0 &       0 &       0 &       0 &       0 &       2 &       7 &     120 &     300 &     480 &     690 &   1'050 \\ \hline
    R\&D subsidy                       & &      0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 \\ \hline
    Initial capital                    & &     45 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 \\ \hline
    Capital contribution/Loans         & &    350 &   5'000 &       0 &       0 &       0 &       0 &       0 &   5'000 &       0 &       0 &       0 &       0 \\ \hline
    Other income                       & &      0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 &       0 \\ \hline
                                       & &        &         &         &         &         &         &         &         &         &         &         &         \\ \hline
    Total Cash Inflows ($B$)           & &    395 &   5'000 &       0 &       0 &       0 &       2 &       7 &   5'120 &     300 &     480 &     690 &   1'050 \\ \hline
    {\bf Cash Outflows}                & &        &         &         &         &         &         &         &         &         &         &         &         \\ \hline
    Personnel costs                    & &     47 &     128 &     373 &     518 &     644 &     685 &     712 &     763 &     776 &     770 &     770 &     770 \\ \hline
    External costs                     & &     20 &      49 &      37 &      64 &      69 &      72 &      72 &      76 &      75 &      57 &      57 &      57 \\ \hline
                                       & &        &         &         &         &         &         &         &         &         &         &         &         \\ \hline
    Investments                        & &      2 &       6 &       6 &       6 &       6 &       6 &       6 &       6 &       6 &       6 &       6 &       6 \\ \hline
    Other expenses                     & &     28 &      88 &      88 &      76 &      95 &     109 &     168 &     174 &     174 &     173 &     171 &     171 \\ \hline
                                       & &        &         &         &         &         &         &         &         &         &         &         &         \\ \hline
    Total Cash Outflows ($C$)          & &     97 &     503 &     503 &     664 &     814 &     872 &     958 &   1'019 &   1'031 &   1'006 &   1'004 &   1'004 \\ \hline
    Ending Cash Balance ($A+B-C$)      & &    298 &   4'561 &   4'561 &   3'898 &   3'084 &   2'214 &   1'263 &   5'364 &   4'633 &   4'107 &   3'794 &   3'840 \\
  \end{tabular}
}
\end{frame}


\begin{frame}
  \begin{center}
    \includegraphics[width=0.66\textwidth]{taler-logo-2017.pdf}
  \end{center}
\end{frame}


\end{document}
